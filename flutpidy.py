from typing import Tuple
import io
import imghdr
import time
import random
import socket

from mopidyapi import MopidyAPI
from PIL import Image, ImageDraw
from requests import get


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(("192.168.0.96", 1337))
print("pixelflut connected")

m = MopidyAPI(host="192.168.0.120")
print("mopidy connected")

current_payload = bytes()


@m.on_event("track_playback_started")
def changed_track(event):
    track = event.tl_track.track
    new_track(track)


def new_track(track):
    print(f"Got new track: {track}")

    cover_image_bs, cover_width, cover_height = cover_image(track, offset=(0, 500))
    text_image_bs = text_image(track, offset=(cover_width, 500))

    # send it off
    global current_payload
    current_payload = text_image_bs + cover_image_bs


def cover_image(track, offset=(0, 0)) -> Tuple[bytes, int, int]:
    # make cover art image
    albumuri = track.album.uri
    covers = m.library.get_images([albumuri])[albumuri]
    if not covers:
        print("No covers :(")
        return bytes(), 0, 0

    smallest = min(covers, key=lambda i: i.width)
    if not smallest.uri.startswith("http"):
        print("Cover uri not http :(")
        return bytes(), 0, 0

    r = get(smallest.uri)
    if not r.status_code == 200:
        print("HTTP error on cover :(")
        return bytes(), 0, 0

    imgcodec = imghdr.what("", h=r.content)
    if imgcodec not in ("jpeg", "png"):
        print(f"Unknown codec {imgcodec} :(")
        return bytes(), 0, 0

    # load into PIL
    image = Image.open(io.BytesIO(r.content))
    pixels = image.load()

    imgbytes = bytes()
    for x in range(smallest.width):
        for y in range(smallest.height):
            r, g, b = pixels[x, y]
            pixelstr = f"PX {x+offset[0]} {y+offset[1]} {r:02x}{g:02x}{b:02x}\n"
            imgbytes += pixelstr.encode("utf8")
    print("Got cover!")
    return imgbytes, smallest.width, smallest.height


def text_image(track, offset=(0, 0)) -> bytes:
    # track parsing
    trackname = track.name
    trackartist = ", ".join(a.name for a in track.artists)
    trackstr = f"'{trackname}' by {trackartist}"

    # make text image
    size = (10 + 8*len(trackstr), 64)
    width, height = size
    bg_color = (random.randint(0, 64), random.randint(0, 64), random.randint(0, 64))
    img = Image.new('RGB', size, color=bg_color)
    d = ImageDraw.Draw(img)
    d.text((10,10), "Currently playing", fill=(196, 196, 196))
    d.text((10,30), trackstr, fill=(196, 196, 196))
    pixels = img.load()

    # make pixelflut payload
    imgbytes = bytes()
    for x in range(width):
        for y in range(height):
            r, g, b = pixels[x, y]
            pixelstr = f"PX {x+offset[0]} {y+offset[1]} {r:02x}{g:02x}{b:02x}\n"
            imgbytes += pixelstr.encode("utf8")
    print("Made text image")
    return imgbytes


# raw initial track
current = m.playback.get_current_track()
new_track(current)

while True:
    s.send(current_payload)
