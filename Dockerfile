FROM python:3.8
RUN pip install mopidyapi pillow requests
COPY flutpidy.py /
CMD python flutpidy.py
