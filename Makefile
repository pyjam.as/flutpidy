NAME=flutpidy

docker-run: docker-build
	docker run -t $(NAME)

docker-build:
	docker build . -t $(NAME)
